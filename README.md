# Ambiente de Auto-Evaluación Virtual (AAEV) #

**Abstract** — In recent years there was an increase in the use of technology for teaching classes to the point that the student has the possibility of taking non-face (virtual) subjects therefore, the online assessments were became in increasingly demanded resource. The objective of this survey is to evaluate the possibility to implement an automated way of the creation, implementation and qualification process of electronic assessments.

### Details ###

* Estimated completion date December, 2015.
* Developed on **Python** under **Django** framework using **MVT** pattern (Model-View-Template)
* Front-end using **HTML**, **CSS** (with Bootstrap), **JQuery** and **AJAX**
* **MySQL** database

### Collaborators ###
* Khusnulina Valeria: Developer
* Zavaglia Matias: Documentation writer 

### DER ###
![Diagrama_entidad_relacion.png](https://bitbucket.org/repo/j5Gg6G/images/1074296818-Diagrama_entidad_relacion.png)

### Deployment diagram ###
![Diagrama_de_despiegue.png](https://bitbucket.org/repo/j5Gg6G/images/3181736905-Diagrama_de_despiegue.png)

### PRINTS ###
![login.png](https://bitbucket.org/repo/j5Gg6G/images/2559272321-login.png)
![materia.png](https://bitbucket.org/repo/j5Gg6G/images/3653275655-materia.png)
![usuarios.png](https://bitbucket.org/repo/j5Gg6G/images/2688065981-usuarios.png)