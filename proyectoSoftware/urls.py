from django.conf.urls import patterns, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# ------- Home
urlpatterns = patterns( 'autoevaluaciones.views.inicioView',
    ( r'^$', 'home' ),
    ( r'^nuevo_administrador$', 'nuevoAdministrador' ),
    )

# ------- Usuarios
urlpatterns += patterns( 'autoevaluaciones.views.usersView',
    ( r'^nuevos_usuarios/$', 'new_users' ),
    ( r'^nuevos_usuarios/upload-users-csv/$', 'read_csv' ),
    ( r'^nuevos_usuarios/upload-users/$', 'add_users' ),
    ( r'^lista_usuario/$', 'lista_usuario' ),
    )

# ------- Materias
urlpatterns += patterns( 'autoevaluaciones.views.materiasView',
    ( r'^nueva_materia/$', 'nueva_materia' ),
    ( r'^lista_materia/$', 'lista_materia' ),
    )

# ------- Temas
urlpatterns += patterns( 'autoevaluaciones.views.temasView',
    ( r'^asignar_tema/$', 'getIndex' ),
    ( r'^asignar_tema/temas_de_materia/(\d{1,5})/$', 'temas_de_materia' ),
	( r'^asignar_tema/temas_de_materia_combo/(\d{1,5})/$', 'temas_de_materia_combo' ),
    ( r'^crear_tema/$', 'asignar_tema' ),
    )

# ------- Links de prueba
urlpatterns += patterns( 'autoevaluaciones.views.controlador',
    ( r'^time/$', 'current_datetime' ),
    ( r'^time/plus/(\d{1,2})/$', 'hours_ahead' ),
    )

# ------- Preguntas
urlpatterns += patterns('autoevaluaciones.views.preguntasView',
    ( r'^preguntas/$', 'get_index' ),
    ( r'^preguntas/preguntas_del_tema/(\d{1,5})/$', 'preguntas_incompatibles' ),
    ( r'^respuestas/form/(\d{1,5})/$', 'respuestas_form' ),
    ( r'^nueva_pregunta/$', 'nueva_pregunta' ),
	( r'^preguntas/index/$', 'lista_pregunta' ),
    ( r'^preguntas/lista/(?P<tema_id>\d{1,5})/$', 'lista_pregunta' ),
    ( r'^preguntas/lista/(?P<tema_id>\d{1,5})/(?P<materia_id>\d{1,5})/$', 'lista_pregunta' ),
)

# ------- Evaluaciones
urlpatterns += patterns( 'autoevaluaciones.views.evaluacionesView',
    ( r'^evaluaciones/$', 'get_index' ),
    ( r'^evaluacion/ver/(\d{1,5})/$', 'mostrar_evaluacion' ),
    ( r'^nueva_evaluacion/$', 'nueva_evaluacion_index' ),
    ( r'^nueva_evaluacion/crear/$', 'nueva_evaluacion' ),
    ( r'^nueva_evaluacion/configuraciones/$', 'nueva_evaluacion_configuraciones' ),
    ( r'^evaluaciones/temas_materia_checkbox/(\d{1,5})/$', 'temas_materia_checkbox' ),
    )

# ------- Login & Logout
urlpatterns += patterns( '',
    url( r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'} ),
    )
urlpatterns += patterns( 'autoevaluaciones.views.auth.view_auth',
    url( r'^accounts/logout/$', 'logout' ),
    )

# ------- Archivos estaticos (css, js e imagenes)
urlpatterns += staticfiles_urlpatterns()