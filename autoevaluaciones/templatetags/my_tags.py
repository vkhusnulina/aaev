'''
Created on 21/09/2014

@author: Valeria
'''

# my_tags.py
from django import template
register = template.Library()
@register.simple_tag
def active( request, pattern ):
    import re
    if re.search( pattern, request.path ):
        return 'active'
    return ''

@register.filter
def lookup(d, key):
    return d[key-1]