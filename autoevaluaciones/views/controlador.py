# Create your views here.
from django.shortcuts import render_to_response
import datetime
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib.auth.models import User

def current_datetime( request ):
#     user = User.objects.create_user( 'root', 'vkhusnulina@gmail.com', 'root' )
#     user.last_name = 'Khusnulina'
#     user.save()

    now = datetime.datetime.now()
    return render_to_response( 'current_time.html', {'current_date' : now}, context_instance = RequestContext( request ) )

@login_required
def hours_ahead( request, offset ):
    offset = int( offset )
    dt = datetime.datetime.now() + datetime.timedelta( hours = offset )
    return render_to_response( 'next_time.html', {'hour_offset':offset, 'next_time':dt}, context_instance = RequestContext( request ) )
