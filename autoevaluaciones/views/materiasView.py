'''
Created on 22/09/2014

@author: Valeria
'''
from django.template import RequestContext

#from forms import FormNuevaMateria
from django.shortcuts import render_to_response

from autoevaluaciones.models import Materia
from django.contrib.auth.models import User

def nueva_materia ( request ):
	if request.method=='POST':
		print "CREAR MATERIA"
		print request.POST
		newMateria = Materia(codigo=request.POST.get('inputCode'), nombre=request.POST.get('inputName'), descripcion=request.POST.get('inputDescription'), creador_id = request.user.id)
		newMateria.save()
		if request.POST.get('inputEsProfesor') == 'on':
			docente = User.objects.get( id = request.user.id )
			newMateria.docentes.add( docente )
	materias = Materia.objects.all()
	return render_to_response( 'materias/lista_materia.html', {'materias' : materias}, context_instance = RequestContext( request ) )

def lista_materia ( request ):
	if request.method=='POST':
		if request.POST.get('inputAction') == 'baja':
			delMateria = Materia.objects.get(id=request.POST.get('inputId'))
			delMateria.delete()
		if request.POST.get('inputAction') == 'mod':
			modMateria = Materia.objects.get(id=request.POST.get('inputId'))
			modMateria.codigo = request.POST.get('inputCodigo')
			modMateria.nombre = request.POST.get('inputNombre')
			modMateria.descripcion = request.POST.get('inputDescripcion')
			modMateria.save()
	materias = Materia.objects.all()
	return render_to_response( 'materias/lista_materia.html', {'materias' : materias}, context_instance = RequestContext( request ) )