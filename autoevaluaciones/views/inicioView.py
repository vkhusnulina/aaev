# # views.py
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.contrib.auth.models import User


@login_required
def home( request ):
	if User.objects.count() > 0:
		return render_to_response( 'home.html', context_instance = RequestContext( request ) )
	else:
		return redirect('/nuevo_administrador')
def nuevoAdministrador( request ):
	if User.objects.count() > 0:
		return redirect('/')
	else:
		return render_to_response( "nuevo_usuario_administrador.html", context_instance = RequestContext( request ) )
