'''
Created on 21/09/2014

@author: Valeria
'''

from django.contrib.auth import logout as contrib_logout
from django.shortcuts import redirect

def logout( request ):
    contrib_logout( request )
    return redirect( '/', permanent = True )
