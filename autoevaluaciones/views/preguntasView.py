from django.template import RequestContext

#from forms import FormNuevaMateria
from django.shortcuts import render_to_response
from django.http.response import HttpResponse
from django.shortcuts import redirect
try:
    from django.utils import simplejson as json
except:
	import json as json

from autoevaluaciones.models import Materia
from autoevaluaciones.models import Tema
from autoevaluaciones.models import Pregunta
from autoevaluaciones.models import TipoPregunta
from autoevaluaciones.models import Respuesta

def preguntas_incompatibles ( request, tema_id ):
	preguntas = []
	preguntas.extend( Pregunta.objects.filter( tema_id = tema_id ) )
	return render_to_response( 'preguntas/tabla_preguntas.html', {'preguntas': preguntas}, context_instance = RequestContext( request ) )

def nueva_pregunta ( request ):
	incompatibles = request.POST.get('preguntas_incompatibles')
	incompatibles = incompatibles.split('&')
	nuevaPregunta = (Pregunta.objects.create(tipo_id=request.POST.get('tipo_id'), 
                                tema_id=request.POST.get('tema_id'), 
                                descripcion=request.POST.get('enunciado'), 
                                urlImagen='',
                                ultimoUso= 0
                                ))
	if request.POST.get('preguntas_incompatibles') != '':
		for inc_id in incompatibles:
			nuevaPregunta.preguntasIncompatibles.add(inc_id)
			nuevaPregunta.save()
	cantidad_respuestas = request.POST.get('cantidad_respuestas')
	for x in range( 1, int(cantidad_respuestas) + 1 ):
		respuesta = (Respuesta.objects.create(pregunta_id = nuevaPregunta.id, 
								descripcion = request.POST.get('respuesta' + str(x)),
								url_imagen = "",
								correcta = int(request.POST.get('valor' + str(x)))
								))
	response_data = {}
	response_data['errors'] = "sin errores"
	return HttpResponse( json.dumps( response_data ), content_type = "application/json" )

def get_index ( request ):
	materias = Materia.objects.all()
	tiposPregunta = TipoPregunta.objects.all()
	return render_to_response( 'preguntas/index.html', {'materias' : materias, 'materia_seleccionar' : materias[0].id, 'tiposPregunta' : tiposPregunta}, context_instance = RequestContext( request ) )

def respuestas_form ( request, pregunta_id):
	respuestas = Respuesta.objects.filter( pregunta_id = pregunta_id)
	pregunta = Pregunta.objects.get( pk = int(pregunta_id) )
	if(pregunta.tipo.id == 4):
		editar = False
	else:
		editar = True
	return render_to_response( 'preguntas/respuestas_form.html', {'respuestas' : respuestas, 'editar' : editar}, context_instance = RequestContext( request ) )

def lista_pregunta ( request, tema_id = '0', materia_id = '0' ):
	if request.method=='POST':
		print "POST !!!!"
		pregunta = Pregunta.objects.get(id=request.POST.get('inputId'))
		tema_id = pregunta.tema.id
		if request.POST.get('inputAction') == 'baja':
			pregunta.delete()
		if request.POST.get('inputAction') == 'mod':
			pregunta.descripcion = request.POST.get('inputDescripcion')
			pregunta.ultimoUso = 0
			pregunta.save()
			for respuesta in Respuesta.objects.filter( pregunta_id = pregunta.id):
				respuesta.descripcion = request.POST.get('edit_respuesta' + str(respuesta.id))
				if request.POST.get('edit_respuesta_value' + str(respuesta.id)) == 'on':
					respuesta.correcta = 1
				else:
					respuesta.correcta = 0
				respuesta.save()
		materias = Materia.objects.all()
		tiposPregunta = TipoPregunta.objects.all()
		tema = Tema.objects.get( pk = int(tema_id))
		print tema.nombre
		return render_to_response( 'preguntas/index.html', {'materias' : materias, 'materia_seleccionar' : tema.materia.id, 'tiposPregunta' : tiposPregunta}, context_instance = RequestContext( request ) )
	else:
		preguntas = []
		if materia_id != '0':
			for tema in Tema.objects.filter( materia_id = int( materia_id ) ):
				preguntas.extend( Pregunta.objects.filter( tema_id = tema.id) )
			return render_to_response( 'preguntas/lista_pregunta.html', {'preguntas': preguntas}, context_instance = RequestContext( request ) )
		elif tema_id != '0':
			preguntas.extend( Pregunta.objects.filter( tema_id = tema_id ) )
			return render_to_response( 'preguntas/lista_pregunta.html', {'preguntas': preguntas}, context_instance = RequestContext( request ) )
		else:
			return redirect('/preguntas/')