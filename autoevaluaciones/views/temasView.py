'''
Created on 22/09/2014

@author: M
'''
from django.template import RequestContext

# from forms import FormNuevaMateria
from django.shortcuts import render_to_response

from autoevaluaciones.models import Tema
from autoevaluaciones.models import Materia
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse
import json

@csrf_exempt
def asignar_tema( request ):
    errors = {}
    materia = Materia.objects.get( pk = int( request.POST.get( 'idMateria' ) ) )
    newTema = Tema( materia = materia, nombre = request.POST.get( 'name' ), descripcion = request.POST.get( 'description' ) )
    newTema.save()
    response_data = {}
    response_data['errors'] = errors
    return HttpResponse( json.dumps( response_data ), content_type = "application/json" )


def getIndex ( request ):
    materias = Materia.objects.all()
    # temas = Tema.objects.filter(materia_id=request.POST['valor'])
    return render_to_response( 'temas/agregar_tema.html', {'materias' : materias}, context_instance = RequestContext( request ) )

def temas_de_materia ( request, materia_id ):
    temas = Tema.objects.filter( materia_id = int( materia_id ) )
    return render_to_response( 'temas/temas_de_materia.html', {'temas' : temas, 'materia_id': materia_id}, context_instance = RequestContext( request ) )

def temas_de_materia_combo ( request, materia_id ):
    temas = Tema.objects.filter( materia_id = int( materia_id ) )
    return render_to_response( 'temas/temas_de_materia_combo.html', {'temas' : temas}, context_instance = RequestContext( request ) )

