from django.template import RequestContext

# To Response
from django.shortcuts import render_to_response
from django.http.response import HttpResponse
from django.shortcuts import redirect
from django.template.loader import render_to_string

# Python library
import random
from random import shuffle
import time

# Model
from autoevaluaciones.models import Materia
from autoevaluaciones.models import Tema
from autoevaluaciones.models import Pregunta
from autoevaluaciones.models import TipoPregunta
from autoevaluaciones.models import Respuesta
from autoevaluaciones.models import Evaluacion

def get_index ( request ):
	materias = Materia.objects.all()
	evaluaciones = Evaluacion.objects.all()
	return render_to_response( 'evaluaciones/index.html', {'materias' : materias, 'evaluaciones' : evaluaciones}, context_instance = RequestContext( request ) )

def nueva_evaluacion_index ( request ):
	materias = Materia.objects.all()
	return render_to_response( 'evaluaciones/nueva_evaluacion_index.html', {'materias' : materias}, context_instance = RequestContext( request ) )

def nueva_evaluacion_configuraciones ( request ):
	temas_str = request.POST.get('temas')
	tipos = TipoPregunta.objects.all()
	preguntas = []
	for tipo in tipos:
		preguntas.insert(tipo.id - 1, Pregunta.objects.filter(tema_id__in = temas_str.split(','), tipo_id = tipo.id))
		print preguntas[tipo.id - 1].count()
	return render_to_response('evaluaciones/configuraciones.html', {'temas' : temas_str, \
																	'tipos' : tipos, \
																	'preguntas' : preguntas}, \
																	context_instance = RequestContext( request ))

def temas_materia_checkbox( request, materia_id ):
	temas = Tema.objects.filter( materia_id = int( materia_id ) )
	return render_to_response( 'temas/temas_materia_checkbox.html', {'temas' : temas}, context_instance = RequestContext( request ) )

def nueva_evaluacion ( request ):
	'''
	- Alta de Evaluacion misma
	'''
	datatime = request.POST.get('inputDate')
	datatime = str(datatime[6:]) + "-" + str(datatime[3:5]) + "-" + str(datatime[:2]) + " " \
				+ request.POST.get('inputHour') + ":" + request.POST.get('inputMinute') + ":00"
	date_now = time.strftime('%Y-%m-%d')
	evaluacion = (Evaluacion.objects.create(nombre=request.POST.get('inputName'), 
                                fecha_creacion=date_now, 
                                fecha_hora_comienzo=datatime, 
                                duracion_minutos=int(request.POST.get('inputMaxMinutes')),
                                acepta_entrega_tardia= int(request.POST.get('inputAccept'))
                                ))
	'''
	- Seleccion aleatoria (con prioridades) de Preguntas para la evaluacion
	- Cantidad de preguntas por tipo de pregunta sujeto a su disponibilidad
	y presencia de preguntas incompatibles
	- En consola se imprime el resumen de la aleatoriedad
	'''
	temas_str = request.POST.get('temas')
	tipos = TipoPregunta.objects.all()
	preguntas_seleccionadas = []
	preguntas_incompatibles = []
	print "*************************************************"
	for tipo in tipos:
		preguntas = []
		print "-----------------------------------------------"
		print "Tipo " + tipo.tipo
		for x in range(0, int(request.POST.get('tipo' + str(tipo.id)))):
			preguntas_aleatorias = []
			preguntas = Pregunta.objects.filter(tema_id__in = temas_str.split(','), tipo_id = tipo.id).order_by('-ultimoUso').exclude(id__in=preguntas_incompatibles)
			cantidad_preguntas = preguntas.count()
			if cantidad_preguntas > 0:
				mayor_aparicion = preguntas[0].ultimoUso
				print "Mayor aparicion " + str(mayor_aparicion)
				print "Cantidad de preguntas: " + str(cantidad_preguntas)
				for pregunta in preguntas:
					cantidad_a_agregar = mayor_aparicion - pregunta.ultimoUso + 1
					preguntas_aleatorias.extend([pregunta] * cantidad_a_agregar)
				max_random = len(preguntas_aleatorias) - 1
				pregunta_seleccionada = random.randint(0, max_random)
				print "Random " + str(preguntas_aleatorias[pregunta_seleccionada].id)
				print "Preguntas Aleatorias " + str(len(preguntas_aleatorias))
				preguntas_seleccionadas.append(preguntas_aleatorias[pregunta_seleccionada])
				preguntas_incompatibles.append(preguntas_aleatorias[pregunta_seleccionada].id)
				incompatibles = Pregunta.objects.filter(preguntasIncompatibles__in=[preguntas_aleatorias[pregunta_seleccionada].id])
				for incompatible in incompatibles:
					preguntas_incompatibles.append(incompatible.id)
				print "Preguntas Incompatibles"
				print preguntas_incompatibles
	print "Preguntas Seleccionadas " + str(len(preguntas_seleccionadas))
	print "*************************************************"
	for add_pregunta in preguntas_seleccionadas:
		evaluacion.preguntas.add(add_pregunta.id)
		print add_pregunta.id
		evaluacion.save()
		add_pregunta.ultimoUso += 1
		add_pregunta.save()
	return redirect('/evaluacion/ver/' + str(evaluacion.id))

def mostrar_evaluacion( request, evaluacion_id):
	evaluacion = Evaluacion.objects.get( pk = int(evaluacion_id) )
	tipos = TipoPregunta.objects.all()
	preguntas = []
	respuestas_join = []
	for tipo in tipos:
		preguntas.insert(tipo.id - 1, [])
	print preguntas
	for pregunta in evaluacion.preguntas.all():
		preguntas[pregunta.tipo.id -1].append([pregunta, Respuesta.objects.filter(pregunta_id = pregunta.id)])
		if(pregunta.tipo.id == 3):
			respuestas_join.extend(Respuesta.objects.filter(pregunta_id = pregunta.id))
	shuffle(respuestas_join)
	return render_to_response('evaluaciones/ver.html', {'evaluacion' : evaluacion, \
													'tipos' : tipos, \
													'preguntas' : preguntas, \
													'respuestas_join' : respuestas_join},context_instance = RequestContext( request ))
