'''
Created on 22/09/2014

@author: Valeria
'''
import csv
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse
from django.contrib.auth.models import User
try:
    from django.utils import simplejson as json
except:
    import json as json
from autoevaluaciones.models import Materia, Perfil

def new_users ( request ):
    materias = Materia.objects.all()
    return render_to_response( 'users/add_users.html', {'materias' : materias}, context_instance = RequestContext( request ) )

@csrf_exempt
def read_csv( request ):
    rows = {}
    errors = {}
    if request.FILES:
        file = request.FILES.get( 'file_upload' )
        reader = csv.reader( file, delimiter = ';', quoting = csv.QUOTE_NONE )
        i = 0
        list = 0
        for row in reader:
            try:
                rows[i] = {}
                rows[i]["dni"] = row[0]
                rows[i]["firstName"] = row[1]
                rows[i]["lastName"] = row[2]
                rows[i]["email"] = row[3]
                i = i + 1
            except Exception:
                errors[list] = 'Error en la linea "' + ';'.join( row ) + '"'
                pass
            list += 1
    response_data = {}
    response_data['users'] = rows
    response_data['errors'] = errors
    return HttpResponse( json.dumps( response_data ), content_type = "application/json" )

@csrf_exempt
def add_users( request ):
    errors = {}
    asignar = request.POST.get( 'asignarAlumnosAMateria' )
    if request.POST.get( 'reemplazarDatosDelAlumno' ):
        reemplazar = True
    else:
        reemplazar = False
    i = request.POST.get( 'cantidad' )
    i = i.split( "_" )
    j = 0
    for x in range( 1, len( i ) ):
        if User.objects.filter( email = request.POST.get( 'mail' + i[x] ) ).exists():
            errors[j] = "El usuario con el email " + request.POST.get( 'mail' + i[x] ) + " ya se encuentra registrado"
            j = j + 1
        else:
            user = User.objects.create_user( username = "DNI" + request.POST.get( 'dni' + i[x] ), email = request.POST.get( 'mail' + i[x] ), password = request.POST.get( 'dni' + i[x] ), first_name = request.POST.get( 'name' + i[x] ), last_name = request.POST.get( 'last' + i[x] ) )
            profile = Perfil.objects.create( user = user, dni = request.POST.get( 'dni' + i[x] ) )
    # print request.body
    response_data = {}
    response_data['errors'] = errors
    return HttpResponse( json.dumps( response_data ), content_type = "application/json" )

def lista_usuario ( request ):
    if request.method=='POST':
        if request.POST.get('inputAction') == 'baja':
            delUsuario = User.objects.get(id=request.POST.get('inputId'))
            delUsuario.delete()
        if request.POST.get('inputAction') == 'mod':
            modUsuario = User.objects.get(id=request.POST.get('inputId'))
            modUsuario.first_name = request.POST.get('inputNombre')
            modUsuario.last_name = request.POST.get('inputApellido')
            modUsuario.username = request.POST.get('inputDni')
            modUsuario.email = request.POST.get('inputMail')
            modUsuario.save()
    usuarios = User.objects.all()
    return render_to_response( 'users/lista_usuario.html', {'usuarios' : usuarios}, context_instance = RequestContext( request ) )
