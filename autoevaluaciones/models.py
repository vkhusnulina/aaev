from django.db import models
from django.contrib.auth.models import User

class Permiso ( models.Model ):
    solapa = models.CharField( max_length = 100 )
    descripcion = models.CharField( max_length = 100 )
    class Meta:
        db_table = 'permisos'

class Grupo ( models.Model ):
    nombre = models.CharField( max_length = 80 )
    permisos = models.ManyToManyField( Permiso, related_name = 'permiso' , db_table = 'grupo_permiso' )
    class Meta:
        db_table = 'grupos'

class Perfil ( models.Model ):
    usuario = models.OneToOneField( User )
    grupo = models.ForeignKey( Grupo )
    dni = models.CharField( max_length = 50, unique = True )
    telefono = models.CharField( max_length = 50 )
    class Meta:
        db_table = 'perfiles'

class TipoPregunta ( models.Model ):
    tipo = models.CharField( max_length = 100 )
    enunciado = models.CharField( max_length = 200 )
    def __unicode__( self ):
        return self.tipo
    class Meta:
        db_table = 'tipos_pregunta'

class Materia ( models.Model ):
    codigo = models.IntegerField()
    creador = models.ForeignKey( User, related_name = 'usuario_creador' )
    nombre = models.CharField( max_length = 100 )
    descripcion = models.TextField()
    docentes = models.ManyToManyField( User, related_name = 'docente', db_table = 'materia_docente' )
    alumnos = models.ManyToManyField( User, related_name = 'alumno' , db_table = 'materia_alumno' )
    class Meta:
        db_table = 'materias'

class Tema ( models.Model ):
    materia = models.ForeignKey( Materia )
    nombre = models.CharField( max_length = 100 )
    descripcion = models.TextField()
    class Meta:
        db_table = 'temas'

class Pregunta ( models.Model ):
    tipo = models.ForeignKey( TipoPregunta )
    tema = models.ForeignKey( Tema )
    descripcion = models.TextField()
    urlImagen = models.URLField()
    ultimoUso = models.IntegerField(default = 0)
    #from_pregunta_id, to_pregunta_id
    preguntasIncompatibles = models.ManyToManyField( "self", related_name = 'to_pregunta', db_table = 'preguntas_incompatibles' )
    class Meta:
        db_table = 'preguntas'

class Evaluacion ( models.Model ):
    nombre = models.CharField( max_length = 50 )
    fecha_creacion = models.DateField()
    fecha_hora_comienzo = models.DateTimeField()
    duracion_minutos = models.IntegerField()
    acepta_entrega_tardia = models.BooleanField()
    preguntas = models.ManyToManyField( Pregunta , db_table = 'evaluacion_pregunta' )
    class Meta:
        db_table = 'evaluaciones'

class EvaluacionesUsuarios( models.Model ):
    evaluacion = models.ForeignKey( Evaluacion )
    usuario = models.ForeignKey( User )
    clave_acceso = models.CharField( max_length = 50 )
    fecha_hora_comienzo = models.DateTimeField()
    fecha_hora_final = models.DateTimeField()
    nota_final = models.FloatField()
    class Meta:
        db_table = 'evaluaciones_usuarios'

class Respuesta ( models.Model ):
    pregunta = models.ForeignKey( Pregunta )
    descripcion = models.TextField()
    url_imagen = models.URLField()
    correcta = models.BooleanField( default = False )
    class Meta:
        db_table = 'respuestas'

class EvaluacionPreguntaRespuestaUsuario( models.Model ):
    pregunta = models.ForeignKey( Pregunta )
    respuesta = models.ForeignKey( Respuesta, blank = True )
    respuesta_texto = models.TextField()
    class Meta:
        db_table = 'evaluacion_pregunta_respuesta_usuario'
